FROM node:22-alpine@sha256:9bef0ef1e268f60627da9ba7d7605e8831d5b56ad07487d24d1aa386336d1944 as builder

WORKDIR /app

COPY /app/doc ./doc
COPY /app/src ./src
COPY /app/.env /app/package.json /app/tsconfig.json /app/yarn.lock ./

RUN yarn --ignore-optional && yarn tsc

FROM node:22-alpine@sha256:9bef0ef1e268f60627da9ba7d7605e8831d5b56ad07487d24d1aa386336d1944 as runner

WORKDIR /app

COPY --from=builder /app/dist ./dist
COPY /app/doc ./doc
COPY /app/.env /app/package.json /app/yarn.lock ./

EXPOSE 8000

RUN yarn --ignore-optional --production

CMD ["node", "dist/index.js"]
