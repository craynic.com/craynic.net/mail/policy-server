import 'reflect-metadata'

import { config } from 'dotenv'
config({ path: __dirname + '/../.env' })

import './di'
import app from './app'
import debug from 'debug'
import { Server } from 'http'
import { AddressInfo } from 'net'
import { AppLogger } from './AppLogger'
import { container } from 'tsyringe'

const port: number = parseInt(process.env.APP_PORT ?? '8000'),
    server: Server = app.listen(port)

server.on('listening', (): void => {
    const addr: string | AddressInfo | null = server.address()
    const bind: string = typeof addr === 'string'
      ? 'pipe ' + addr
      : addr === null ? '' : 'port ' + addr.port

    debug('app')('Listening on ' + bind)

    container.resolve(AppLogger).info(`Server is listening on port ${port}`)
})
