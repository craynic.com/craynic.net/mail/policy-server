import express, { Express, NextFunction, Request, Response } from 'express'
import path from 'path'
import createHttpError from 'http-errors'

import statusRouter from './routes/status'
import dovecotRouter from "./routes/dovecot";
import { jsonErrorHandler } from './middleware/jsonErrorHandler'
import * as OpenApiValidator from 'express-openapi-validator'
import { AppLogger } from "./AppLogger";
import { container } from "tsyringe";

const app: Express = express()
const appLogger: AppLogger = container.resolve(AppLogger);

// view engine setup
app
  .use(express.json())
  .use(express.urlencoded({ extended: false }))
  .use(OpenApiValidator.middleware({
    apiSpec: './doc/openapi.yaml',
    validateRequests: true,
    validateResponses: true,
    validateApiSpec: true,
    validateSecurity: true,
    validateFormats: true,
  }))
  .use((req: Request, _res: Response, next: NextFunction): void => {
    appLogger.debug(`${req.method} ${req.url} ${req.ip}`)
    next()
  })
  .use('/status', statusRouter)
  .use('/dovecot', dovecotRouter)
  .use('/spec', express.static(path.join(__dirname, '/../doc/openapi.yaml')))
  .use((_req: Request, _res: Response, next: NextFunction) => next(createHttpError(404)))
  // error handlers
  .use(jsonErrorHandler)

export default app
