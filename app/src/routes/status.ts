import express, { Request, Response, Router } from "express";

const router: Router = express.Router()

router.get('/', function (_req: Request, res: Response): void {
    res.json({status: 'OK'})
})

export default router
