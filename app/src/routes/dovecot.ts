import express, { Request, Response, Router } from 'express'
import { jsonAccepted } from '../middleware/jsonAccepted'
import { AppLogger } from "../AppLogger";
import { container } from 'tsyringe'

const router: Router = express.Router()
const appLogger: AppLogger = container.resolve(AppLogger);

type AllowedPolicyCommand = 'allow' | 'command'
type AllowCommandPayload = {
  device_id: string,
  login: string,
  mechanism: string,
  protocol: 'imap',
  pwhash: string,
  remote: string,
  session_id: string,
  tls: boolean,
}
type ReportCommandPayload = AllowCommandPayload & {
  success: boolean,
  policy_reject: boolean,
}

router
  .route('/auth-policy')
  .post(jsonAccepted, (req: Request, res: Response): void => {
    const command: AllowedPolicyCommand = req.query.command as AllowedPolicyCommand

    if (command === 'allow') {
      const payload: AllowCommandPayload = req.body
      appLogger.debug(`Checking access for user "${payload.login}" @ "${payload.remote}"`)
      res.json({status: 0, msg: "Welcome!"})
    } else {
      const payload: ReportCommandPayload = req.body

      if (payload.success) {
        appLogger.info(`Successful access for user "${payload.login}" @ "${payload.remote}"`)
      } else {
        appLogger.warn(`Failed access for user "${payload.login}" @ "${payload.remote}"`)
      }

      res.json({status: 0, msg: "OK"})
    }
  })

export default router
