import * as winston from 'winston'

function createLogger(level: string): winston.Logger {
    const myFormat: winston.Logform.Format = winston.format.printf(
        ({ level, message, label, timestamp, details }) =>
            `${timestamp} [${label}] ${level}: ${message} ${details !== undefined ? JSON.stringify(details) : ''}`,
    )

    return winston.createLogger({
        level: level,
        format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.label({ label: 'logger' }),
            winston.format.prettyPrint(),
            winston.format.colorize(),
            myFormat,
        ),
        transports: new winston.transports.Console(),
    })
}

// noinspection JSUnusedGlobalSymbols
export class AppLogger {
    private logger: winston.Logger

    constructor(level: string) {
        this.logger = createLogger(level)
    }

    public debug(message: string, ...meta: unknown[]): void {
        this.logger.debug(message, ...meta)
    }

    public info(message: string, ...meta: unknown[]): void {
        this.logger.info(message, ...meta)
    }

    public warn(message: string, ...meta: unknown[]): void {
        this.logger.warn(message, ...meta)
    }

    public error(message: string, ...meta: unknown[]): void {
        this.logger.error(message, ...meta)
    }

    public exception(exception: unknown): void {
        if (exception instanceof Array) {
            exception.forEach((exceptionItem: unknown): void => this.exception(exceptionItem))
        } else if (exception instanceof Error) {
            if (exception.stack) {
                this.error(exception.stack)
            }
        } else {
            this.error(JSON.stringify(exception))
        }
    }
}
