import { NextFunction, Request, Response } from 'express'

export function jsonAccepted(req: Request, _res: Response, next: NextFunction): void {
    req.accepts('json') ? next() : next('route')
}
