import { NextFunction, Request, Response } from 'express'
import { HttpError } from 'http-errors'

export function jsonErrorHandler(err: unknown, req: Request, res: Response, next: NextFunction): void {
    if (!req.accepts('json') || res.headersSent) {
        return next(err)
    }

    // set locals, only providing error in development
    const message: string = err instanceof HttpError && err.expose ? err.message : 'Error'

    res.status(err instanceof HttpError ? err.status : 500)
    res.json({ status: 'error', message })
}
