import { container } from 'tsyringe'

import { AppLogger } from './AppLogger'

container.register<AppLogger>(AppLogger, { useValue: new AppLogger('info') })
